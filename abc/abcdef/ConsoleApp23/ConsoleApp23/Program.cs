﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp23
{
    class Foo
    {
        int x;
        public int FooOne
        {
            set { this.x = value * 100; }
            get { return this.x / 50; }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Foo f;
            f = new Foo();
            f.FooOne = 9;
            Console.WriteLine(f.FooOne);
        }
    }
}
