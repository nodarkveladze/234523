﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp24
{
    delegate int MyDel(int x);
    class Program
    {
        static int f(int x)
        {
            return x * x;
        }
        static int g(int x)
        {
            return x * x * x;
        }
        static int h (int x)
        {
            return 2 * x + 1;
        }

        static void Main(string[] args)
        {
            MyDel[] u;
            u = new MyDel[3];
            u[0] = f;
            u[1] = g;
            u[2] = h;
            int result = 0;
            for (int k = 0; k < u.Length; k++)
                result = result + u[k](2);
            Console.WriteLine(result);
        }
    }
}
