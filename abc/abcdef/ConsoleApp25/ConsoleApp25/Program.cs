﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp25
{
    class RectangleException : Exception 
    {
        public double Length { get; private set; }
        public double Width { get; private set; }
        public RectangleException(String txt, double l, double w)
            : base(txt)
        {
            this.Length = l;
            this.Width = w;
        }
    }
    class Rectangle
    {
        double Length;
        double Width;
        public Rectangle(double l, double w)
        {
            if (l < w)
                throw new RectangleException("Length Must be greater or equal to width", l, w);
            if (l <= 0)
                throw new Exception("Length cannot be nonpositive");
            if (w <= 0)
                throw new Exception("Width cannot be nonpositive");
            this.Length = l;
            this.Width = w;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle r;
            try
            {
                try
                {
                    r = new Rectangle(-1, -2);
                    Console.WriteLine("OK");
                }
                catch (RectangleException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    Console.WriteLine("Finally");
                }
                Console.WriteLine("After");

            }
            catch(Exception ex)
            {
                Console.WriteLine("OUTER: " + ex.Message);
            }
        }
    }
}
