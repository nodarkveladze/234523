﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp20
{
    class Parent
    {
        public virtual void Print()
        {
            Console.WriteLine(1000);
        }
    }

    class Child : Parent
    {
        int u;
        public Child(int u)
        {
            this.u = u;
        }
        public override void Print ()
        {
            Console.WriteLine(this.u);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Parent c = new Child(5000);
            c.Print();
        }
    }
}
